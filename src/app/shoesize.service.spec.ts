import { TestBed, inject } from '@angular/core/testing';

import { ShoesizeService } from './shoesize.service';

describe('ShoesizeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShoesizeService]
    });
  });
  it('should ...', inject([ShoesizeService], (service: ShoesizeService) => {
    expect(service).toBeTruthy();
  }));

  it('Service return object ', inject([ShoesizeService], (service: ShoesizeService) => {
    //arrange, act
    var shoe = service.calculateShoeSize(100, 89, 'Toddler', 'uk');
    //assert
    expect(shoe).toBeDefined;
  }));

  it('Service returns false for out of range values', inject([ShoesizeService], (service: ShoesizeService) => {
    //arrange
    var expectedResult = false;
    //act
    var result = service.calculateShoeSize(-11, -11, 'Toddler', 'uk');
    //assert
    expect(result.found).toEqual(expectedResult);
  }));

  it('Service return size 2.5 for toddler uk gauge 100mm 89mm', inject([ShoesizeService], (service: ShoesizeService) => {
    //100, 89 , toddler, Infant 2½F
    //arrange
    var expectedSize = '2½';
    //act
    var shoe = service.calculateShoeSize(100, 89, 'Toddler', 'uk');
    var size = shoe.ukChildSize;
    //assert
    expect(size).toEqual(expectedSize);
  }));

  it('Service return size 2.5 for junior uk gauge 250mm 188mm', inject([ShoesizeService], (service: ShoesizeService) => {
    //250, 188 , junior, Junior 6½F
    //arrange
    var expectedSize = '6½';
    //act
    var shoe = service.calculateShoeSize(250, 188, 'Junior', 'uk');
    var size = shoe.ukChildSize;
    //assert
    expect(size).toEqual(expectedSize);
  }));

  it('Service return width F for toddler uk gauge 100mm 89mm', inject([ShoesizeService], (service: ShoesizeService) => {
    //100, 89 , toddler, Infant 2½F
    //arrange
    var expectedSize = 'F';
    //act
    var shoe = service.calculateShoeSize(100, 89, 'Toddler', 'uk');
    var result = shoe.ukWidthFitting;
    //assert
    expect(result).toEqual(expectedSize);
  }));

  it('Service return width F for junior uk gauge 250mm 188mm', inject([ShoesizeService], (service: ShoesizeService) => {
    //250, 188 , junior, Junior 6½F
    //arrange
    var expectedResult = 'F';
    //act
    var shoe = service.calculateShoeSize(250, 188, 'Junior', 'uk');
    var result = shoe.ukWidthFitting;
    //assert
    expect(result).toEqual(expectedResult);
  }));

  it('Service returns false for valid toddler size when input gauge is junior', inject([ShoesizeService], (service: ShoesizeService) => {
    //arrange
    var expectedResult = false;
    //act
    var result = service.calculateShoeSize(100, 100, 'Junior', 'uk');
    //assert
    expect(result.found).toEqual(expectedResult);
  }));

  it('Service returns Infant 81/5E for  toddler length150 width105 UK', inject([ShoesizeService], (service: ShoesizeService) => {
    //arrange
    var expectedResult = 'Infant 8½E';
    //act
    var shoe = service.calculateShoeSize(150, 105, 'Toddler', 'uk');
    //assert
    expect(shoe.description).toEqual(expectedResult);
  }));

  it('Service returns Infant 81/5E for  toddler length150 width105 US', inject([ShoesizeService], (service: ShoesizeService) => {
    //arrange
    var expectedResult = '9 Narrow';
    var fittingWidth: string;
    var shoeSize: string;
    var AgeRange: string;
    //act
    var shoe = service.calculateShoeSize(150, 105, 'Toddler', 'us');
    //assert
    expect(shoe.description).toEqual(expectedResult);
  }));

    it('Service returns Junior 7 Narrow for  Junior length150 width105 US', inject([ShoesizeService], (service: ShoesizeService) => {
    //arrange
    var expectedResult = '8½ Narrow';
    var fittingWidth: string;
    var shoeSize: string;
    var AgeRange: string;
    //act
    var shoe = service.calculateShoeSize(150, 105, 'Junior', 'US');
    var result = shoe.description;
    //assert
    expect(result).toEqual(expectedResult);
  }));
});


