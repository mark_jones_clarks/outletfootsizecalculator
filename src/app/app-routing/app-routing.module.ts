import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SelectGaugeComponent } from "../select-gauge/select-gauge.component";
import { EnterMeasurementsComponent } from '../enter-measurements/enter-measurements.component';
import { DisplaySizeComponent } from '../display-size/display-size.component';
import {CanActivateDisplaySizeGuard} from '../display-size-guard.service';


const routes: Routes = [
  //for debugging in local directory
  //{ path: '', pathMatch: 'full', redirectTo: 'sizecalculatorexternal/', },
  //{ path: 'sizecalculatorexternal/:region', component: SelectGaugeComponent },
  //for deploying to /sizecalculatorextenral directory
  { path: '', pathMatch: 'full', component: SelectGaugeComponent },
  { path: ':region', component: SelectGaugeComponent },
  { path: 'enter-measurements/:region/:gauge-type', component: EnterMeasurementsComponent },
  { path: 'display-size/:region/:gauge-type/:size', component: DisplaySizeComponent, canActivate :[CanActivateDisplaySizeGuard] }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routableComponents = [
  SelectGaugeComponent,
  EnterMeasurementsComponent,
  DisplaySizeComponent
];