import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorSizecalculatorComponent } from './error-sizecalculator.component';

describe('ErrorSizecalculatorComponent', () => {
  let component: ErrorSizecalculatorComponent;
  let fixture: ComponentFixture<ErrorSizecalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorSizecalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorSizecalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
