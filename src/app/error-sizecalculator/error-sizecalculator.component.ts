import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-error-sizecalculator',
  templateUrl: './error-sizecalculator.component.html',
  styleUrls: ['./error-sizecalculator.component.css']
})
export class ErrorSizecalculatorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
}
  @Input() region: string;

}
