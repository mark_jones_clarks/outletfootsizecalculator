import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppRoutingModule, routableComponents } from './app-routing/app-routing.module';
import { ErrorSizecalculatorComponent } from './error-sizecalculator/error-sizecalculator.component';
import { FooterComponent } from './footer/footer.component';
import {ShoesizeService} from './shoesize.service';
import {CanActivateDisplaySizeGuard} from './display-size-guard.service';
import { DisplaySizeComponent } from './display-size/display-size.component';


@NgModule({
  declarations: [
    AppComponent,
    routableComponents,
    ErrorSizecalculatorComponent,
    FooterComponent,
    DisplaySizeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [ShoesizeService, CanActivateDisplaySizeGuard],
  bootstrap: [AppComponent]
 // bootstrap: [Step1Component]
})
export class AppModule { }
