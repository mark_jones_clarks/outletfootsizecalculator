import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";

import { DisplaySizeComponent } from './display-size.component';
import { FooterComponent } from '../footer/footer.component';

describe('DisplaySizeComponent', () => {
  let component: DisplaySizeComponent;
  let fixture: ComponentFixture<DisplaySizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DisplaySizeComponent,FooterComponent],
      imports: [RouterTestingModule.withRoutes(
        [{ path: 'enter-measurements', component: DisplaySizeComponent }])]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplaySizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
