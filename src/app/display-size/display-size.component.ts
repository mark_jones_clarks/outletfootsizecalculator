import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
@Component({
  selector: 'app-display-size',
  templateUrl: './display-size.component.html',
  styleUrls: ['./display-size.component.css']
})
export class DisplaySizeComponent implements OnInit {

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.gaugeType = this.route.snapshot.params['gauge-type'];
    this.region = this.route.snapshot.params['region'];
    this.SizeDetails = this.route.snapshot.params['size'];
    this.SizeDetailsSplit = this.SizeDetails.split(" ");
    this.SizeType = this.SizeDetailsSplit.shift().toLowerCase();
    this.SizeDetail = this.SizeDetailsSplit.shift().replace("½",".5")
    this.SizeDetailsNoWidth = this.SizeDetail.substr(0, this.SizeDetail.length - 1);
  }
  region:string;
  gaugeType:string;
  SizeDetails :string;
  SizeDetail :string;
  SizeType :string;
  SizeDetailsNoWidth :string;
  SizeDetailsSplit:string[];
}
