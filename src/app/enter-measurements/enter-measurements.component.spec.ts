import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";
import { FormsModule } from '@angular/forms';
import { EnterMeasurementsComponent } from './enter-measurements.component';
import { ErrorSizecalculatorComponent } from '../error-sizecalculator/error-sizecalculator.component';
import { FooterComponent } from '../footer/footer.component';
import {ShoesizeService} from '../shoesize.service';

describe('EnterMeasurementsComponent', () => {
  let component: EnterMeasurementsComponent;
  let fixture: ComponentFixture<EnterMeasurementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EnterMeasurementsComponent,
        ErrorSizecalculatorComponent,
        FooterComponent
      ],
      providers: [ShoesizeService],
      imports: [FormsModule,
        RouterTestingModule.withRoutes(
          [{ path: 'enter-mesurements', component: EnterMeasurementsComponent }])]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterMeasurementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
