import { Component, OnInit } from '@angular/core';
import { ShoesizeService } from '../shoesize.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-enter-measurements',
  templateUrl: './enter-measurements.component.html',
  styleUrls: ['./enter-measurements.component.css']
})
export class EnterMeasurementsComponent implements OnInit {

  constructor(private shoesizeService: ShoesizeService, private route: ActivatedRoute, private router: Router) { }

  region: string;
  gaugeType: string;
  size = {
    length: '',
    width: ''
  };
  sizeDetails: string;
  showError: boolean = false;
  ngOnInit() {
    this.gaugeType = this.route.snapshot.params['gauge-type'];
    this.region = this.route.snapshot.params['region'];
  }

  calculateShoeSize(length, width) {
    var shoe = this.shoesizeService.calculateShoeSize(length, width, this.gaugeType, this.region);
    if (!shoe.found)
    { this.showError = true; }
    else {
      this.sizeDetails = shoe.description;
      this.router.navigate(['/display-size', this.region, this.gaugeType, this.sizeDetails]);
    }
  }
}
