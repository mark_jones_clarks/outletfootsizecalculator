import { TestBed, inject } from '@angular/core/testing';
import {RouterTestingModule} from "@angular/router/testing";
import { CanActivateDisplaySizeGuard } from './display-size-guard.service';

describe('DisplaySizeGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports : [RouterTestingModule],
      providers: [CanActivateDisplaySizeGuard]
    });
  });


  it('should ...', inject([CanActivateDisplaySizeGuard], (service: CanActivateDisplaySizeGuard) => {
    expect(service).toBeTruthy();
  }));
});
