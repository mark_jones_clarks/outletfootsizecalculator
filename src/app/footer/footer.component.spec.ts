import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterComponent } from './footer.component';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

    it('region us should show us-footer', () => {
      component.region = "US";
          fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#us-footer')).toBeDefined();
  });
      it('region amazon should show clarks logo', () => {
      component.region = "amazon";
          fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#ClarksFooterLogo')).toBeDefined();
  });

      it('region amazon should not show us-footer', () => {
      component.region = "amazon";
          fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#us-footer')).toBeNull();
  });
        it('region us should not show clarks logo', () => {
      component.region = "US";
          fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#ClarksFooterLogo')).toBeNull();
  });
});
