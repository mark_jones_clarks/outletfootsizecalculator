import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";
import { ActivatedRoute } from '@angular/router';

import { SelectGaugeComponent } from './select-gauge.component';
import { FooterComponent } from '../footer/footer.component';

class RouterStub {
  snapshot = {
    params: ['region']
  }
  constructor() {
    this.snapshot.params['region'] = 'US';
  }
};

describe('SelectGaugeComponent', () => {
  let component: SelectGaugeComponent;
  let fixture: ComponentFixture<SelectGaugeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SelectGaugeComponent,
        FooterComponent],
      providers: [{ provide: ActivatedRoute, useClass: RouterStub }],
      imports: [
        RouterTestingModule.withRoutes(
          [{ path: 'enter-measurements', component: SelectGaugeComponent }])]
    }).compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(SelectGaugeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should take region code us in any case and set as uppercase', inject([ActivatedRoute], (router: ActivatedRoute) => {
    //arrange
    var expectedResult = 'US';
    //stubbed router has region = US
    //act        
    var result = component.region;
    //assert
    expect(result).toEqual(expectedResult);
  }));
});
