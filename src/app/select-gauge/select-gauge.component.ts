import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-select-gauge',
  templateUrl: './select-gauge.component.html',
  styleUrls: ['./select-gauge.component.css']
})
export class SelectGaugeComponent implements OnInit {

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.region = String(this.route.snapshot.params['region']).toUpperCase();        
    if(!(this.region == 'US' || this.region == 'AMAZON'))
    {
      this.region ='UK';
    }
  }
  region: string; //amazon, us
}
